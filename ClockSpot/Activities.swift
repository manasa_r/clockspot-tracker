//
//  Activities.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/14/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import Foundation


class Activities{

    
    var activities:[String] = []
    
    
    func updateActivities(userProfession:String)->[String]
    {
        
        
        if userProfession == "STUDENT" {
        
           activities = ["Sleep","Study","Eat"]
        
        }
        else if userProfession == "PROFESSOR"{
        
          activities = ["Work", "Teach", "Eat","Exercise", "Research"]
        
        }else if userProfession == "LAWYER"{
        
             activities = ["Work", "Teach", "Eat","Exercise", "Meetings"]
        
        }else if userProfession == "DOCTOR"{
            
            activities = ["Teach", "Eat","Exercise", "Hospital"]
            
        }
       
     return activities
    }


}