//
//  FirstViewController.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/12/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import UIKit

class ActivitiesViewController: UIViewController, OperationProtocol, UITableViewDelegate, UITableViewDataSource  {
    var kinveyOperations:KinveyOperations!
    var activityTimes:[String:String]!
    var activities:[String]!
    var time:String!
    var startTime = NSTimeInterval()
    var timer:NSTimer = NSTimer()
    let defaults = NSUserDefaults.standardUserDefaults()
    var userValue:String!
    var timerLabel:UILabel!
    var subTitleLabel:UILabel!
    var activityTimeTracker:ActivityTimeTracker!
    var updatedActivities:UpdatedActivities!
    var convertedDate:String!
    var currentDate = NSDate()
    var dateFormatter = NSDateFormatter()
    
    @IBOutlet weak var activitiesTableView: UITableView!
    var index:NSIndexPath!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Activities"
        kinveyOperations = KinveyOperations(operationProtocol: self)
        kinveyOperations.retrieveTime()
        activities = []
        activityTimeTracker = ActivityTimeTracker()
        updatedActivities = UpdatedActivities()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if activities != nil{
            return 1
            
        }else{
            return 0
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if activities != nil{
            return activities.count
            
        }else{
            return 0
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func onError(message: String) {
        // displayAlertControllerWithFailure("OOPS!", message:"Login Failed")
        
    }
    func onSuccess() {
        //displayAlertControllerWithTitle("Success", message:"Login Successful")
    }
    func noActiveUser() {
        //
    }
    
    func loginFailed() {
        //
    }
    
    func sendTimeValues(time: ActivityTimeTracker) {
        print(time.activityTimes.keys)
        for activity in time.activityTimes.keys{
            activities.append(activity)
        }
        if activityTimeTracker.activityTimes == nil {
            activityTimeTracker.activityTimes = [:]
        }
        activityTimeTracker.activityTimes = time.activityTimes
        print(activityTimeTracker.activityTimes)
        self.activitiesTableView.reloadData()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("activities", forIndexPath: indexPath) as UITableViewCell
        let activity:UILabel = cell.viewWithTag(19) as! UILabel
        let activityTime:UILabel = cell.viewWithTag(20) as! UILabel
        var activityValue = activities
        let activityImage:UIImageView = cell.viewWithTag(30) as! UIImageView
        userValue = defaults.valueForKey(Constants.USERNAME) as! String
        activityTimeTracker.userName = userValue
        if activities != nil{
            activity.text = activities[indexPath.row]
            activityTime.text = activityTimeTracker.activityTimes[activities[indexPath.row]]!
            let image: UIImage? = UIImage(named: activityValue[indexPath.row])

            if  image == nil{
                print("In If")
                activityImage.image = UIImage(named: "Default.png")
            }else{
                activityImage.image = image!
            }
            let switchDemo =   UISwitch(frame:CGRectMake(50, 50, 0, 0))
            switchDemo.on = false
            switchDemo.setOn(false, animated: false)
            switchDemo.tag = indexPath.row
            switchDemo.addTarget(self, action: "switchValueDidChange:", forControlEvents: .ValueChanged)
            cell.accessoryView = switchDemo
            cell.contentView.addSubview(switchDemo)
        }
        return cell
    }
    
    func disableAllSwitchesExceptForRow(row:Int){
        for i in 0.stride(to: activities.count, by: 1){
            let currentSwitch  = self.activitiesTableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0))!.accessoryView as! UISwitch
            currentSwitch.enabled = row == currentSwitch.tag
        }            
    }
    
    func enableAllSwitches(){
        for i in 0.stride(to: activities.count, by: 1){
            let currentSwitch  = self.activitiesTableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0))!.accessoryView as! UISwitch
            currentSwitch.enabled = true
        }
    }
    
    // called when the user taps on the sswitch in the row ...
    func switchValueDidChange(sender:UISwitch!)
    {
        if (sender.on){
            disableAllSwitchesExceptForRow(sender.tag)
            if (!timer.valid) {
                let aSelector : Selector = "updateTime"
                timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: aSelector, userInfo: nil, repeats: true)
                startTime = NSDate.timeIntervalSinceReferenceDate()
                let indexPath  = NSIndexPath(forRow: sender.tag, inSection: 0)
                let cellView  = self.activitiesTableView.cellForRowAtIndexPath(indexPath)
                let startLabel = cellView?.viewWithTag(21) as! UILabel
                timerLabel = startLabel
            }
        }
        else{
            enableAllSwitches()
            timer.invalidate()
            let indexPath  = NSIndexPath(forRow: sender.tag, inSection: 0)
            let cellView  = self.activitiesTableView.cellForRowAtIndexPath(indexPath)
            subTitleLabel = cellView?.viewWithTag(20) as! UILabel
            
            let time = timerLabel.text!.componentsSeparatedByString(":")
            let currentSeconds = Double(time[2])
            let currentMinutes = Double(time[1])
            let currentHours = Double(time[0])
            
            let value = activityTimeTracker.activityTimes[activities[indexPath.row]]!
            var previousTime = value.componentsSeparatedByString(":")
            let previousHours = Double(previousTime[0])
            let previousMinutes = Double(previousTime[1])
            let previousSeconds = Double(previousTime[2])
            
            var totalHours = Int(currentHours!+previousHours!)
            var totalMinutes = Int(currentMinutes!+previousMinutes!)
            var totalSeconds = Int(currentSeconds!+previousSeconds!)
            
            if totalSeconds >= 60{
                totalMinutes = totalMinutes+1
                totalSeconds = totalSeconds - 60
            }
            if totalMinutes >= 60{
                totalHours = totalHours + 1
                totalMinutes = totalMinutes - 60
            }
            let modifiedHours = String(format: "%02d", totalHours)
            let modifiedMinutes = String(format: "%02d", totalMinutes)
            let modifiedSeconds = String(format: "%02d", totalSeconds)
            subTitleLabel.text = "\(modifiedHours):\(modifiedMinutes):\(modifiedSeconds)"
            activityTimeTracker.activityTimes[activities[indexPath.row]] = "\(modifiedHours):\(modifiedMinutes):\(modifiedSeconds)"
            timerLabel.text = "00:00:00"
            kinveyOperations.trackTime(activityTimeTracker)
        }
    }
    
    func updateTime(){
        if timerLabel != nil {
            let currentTime = NSDate.timeIntervalSinceReferenceDate()
            //Find the difference between current time and start time.
            var elapsedTime: NSTimeInterval = currentTime - startTime
            //calculate the hours in elapsed time.
            let hours = UInt8(elapsedTime / 3600.0)
            elapsedTime -= (NSTimeInterval(hours) * 3600)
            //calculate the minutes in elapsed time.
            let minutes = UInt8(elapsedTime / 60.0)
            elapsedTime -= (NSTimeInterval(minutes) * 60)
            //calculate the seconds in elapsed time.
            let seconds = UInt8(elapsedTime)
            elapsedTime -= NSTimeInterval(seconds)
            
            let strHours = String(format: "%02d", hours)
            let strMinutes = String(format: "%02d", minutes)
            let strSeconds = String(format: "%02d", seconds)
            
            timerLabel.text = "\(strHours):\(strMinutes):\(strSeconds)"
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        if activityTimeTracker.activityTimes != nil{
            self.activitiesTableView.reloadData()
        }
    }
    
    //adding a new activity and reloading the table view - added by Manasa on apr12
    
    @IBAction func addActivity(sender: AnyObject) {
        var alertController:UIAlertController?
        alertController = UIAlertController(title: "Add an Activity",
                                            message: "Enter Activity Name",
                                            preferredStyle: .Alert)
        alertController!.addTextFieldWithConfigurationHandler(
            {(textField: UITextField!) in
                textField.placeholder = "Activity Name"
        })
        let action = UIAlertAction(title: "Submit",
                                   style: UIAlertActionStyle.Default,
                                   handler: {[weak self]
                                    (paramAction:UIAlertAction!) in
                                    if let textFields = alertController?.textFields{
                                        let theTextFields = textFields as [UITextField]
                                        let enteredText = theTextFields[0].text
                                        self!.activities.append(enteredText!)
                                        self?.activityTimeTracker.activityTimes[enteredText!] = "00:00:00"
                                    }
                                    self!.kinveyOperations.trackTime(self!.activityTimeTracker)
                                    self!.activitiesTableView?.reloadData()
            })
        
        let cancel = UIAlertAction(title: "Cancel",
            style: .Cancel,
            handler: {(action) -> Void in
            })

        
        alertController?.addAction(action)
        alertController?.addAction(cancel)
        self.presentViewController(alertController!,
                                   animated: true,
                                   completion: nil)
        
    }
}

