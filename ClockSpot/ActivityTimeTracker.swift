//
//  ActivityTimeTracker.swift
//  ClockSpot
//
//  Created by Manasa Bojja on 09/04/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import Foundation

class ActivityTimeTracker:NSObject{
    
    
    
    var entityId:String?
    
    var userName:String!

    
    var activityTimes:[String:String]!
    
    
    
    init(userName:String, activityTimes:[String:String]){
        
        self.userName = userName
      
        self.activityTimes = activityTimes
        
    }
    override init() {
        super.init()
    }
    
    
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "userName" : "userName",
            "activityTimes":"activityTimes"
         

        ]
    }
    
    
    
        
}