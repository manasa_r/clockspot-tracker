//
//  Clock.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 4/11/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import Foundation


class Clock:NSObject{

    var hours:Double!
    var minutes:Double!
    var seconds:Double!
    var entityId:String?

    
    init(hours:Double,minutes:Double,seconds:Double){
    
       self.hours = hours
        self.minutes = minutes
        self.seconds = seconds
    
    }
    
     override init() {
        super.init()
    }
    
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "hours" : "hours",
            "minutes":"minutes",
            "seconds":"seconds"
        ]
    }

}