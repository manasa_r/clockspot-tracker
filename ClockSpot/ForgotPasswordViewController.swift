//
//  ForgotPasswordViewController.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 4/19/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var emailTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func submitBTN(sender: AnyObject) {
        
        
        let emailID = emailTF.text!
        let validEmailID:Bool = isValidEmail(emailTF.text!)

        if emailID == "" || validEmailID == false{
            self.displayAlertControllerWithFailure("OOPS!", message: " Please enter emailID ")
        }
        KCSUser.sendPasswordResetForUser(emailID, withCompletionBlock: { (succeeded: Bool, error: NSError?) -> Void in
            if error == nil {
                if succeeded { // SUCCESSFULLY SENT TO EMAIL
                    print(" Reset email sent to your inbox :\n")
                    self.displayAlertControllerWithTitle("Successful", message: "Reset email sent to your inbox : \n \(emailID)")
                }
                else { // SOME PROBLEM OCCURED
                    self.displayAlertControllerWithFailure("Forgot Password", message: "something gone wrong")
                }
                
            }
            else {
                //ERROR OCCURED, DISPLAY ERROR MESSAGE
                print(error!.description)
                self.displayAlertControllerWithFailure("OOPS", message: error!.description)
            }
            
        });

        
        
    }
    
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
            message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:  { action in let login =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("loginView") as! LoginViewController
                self.navigationController?.pushViewController(login, animated: true) }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
 
        
    }
    func displayAlertControllerWithFailure(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
            message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:{(action:UIAlertAction)->Void in }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    func isValidEmail(username:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(username)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
