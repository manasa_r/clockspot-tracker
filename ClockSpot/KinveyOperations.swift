//
//  KinveyOperations.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/14/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import Foundation

@objc protocol OperationProtocol {
    func onSuccess()
    func onError(message:String)
    func noActiveUser()
    func loginFailed()
    optional func sendValues(user:User)
    optional func sendTimeValues(time:ActivityTimeTracker)
    optional func retrieveProfile(user:User)
    optional func sendActivityGoals(goal:GoalModelClass)

}

class KinveyOperations {
    
    let store:KCSAppdataStore!
    
    let timeStore:KCSAppdataStore!
    
    
    let activityGoal:KCSAppdataStore!
    let operaionDelegate:OperationProtocol!
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    init(operationProtocol:OperationProtocol){
        self.operaionDelegate = operationProtocol
        
        store = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "ClockSpotUsers",
            KCSStoreKeyCollectionTemplateClass : User.self
            ])
        
        timeStore = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "ActivitiesTime",
            KCSStoreKeyCollectionTemplateClass : ActivityTimeTracker.self
            ])
        
        //        activityStore = KCSAppdataStore.storeWithOptions([
        //            KCSStoreKeyCollectionName : "UpdatedActivities",
        //            KCSStoreKeyCollectionTemplateClass : UpdatedActivities.self
        //            ])
        
        activityGoal = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "ActivityGoal",
            KCSStoreKeyCollectionTemplateClass : GoalModelClass.self
            ])
        
    }
    
    func saveData() {
        if let _ = KCSUser.activeUser() {
        }else{
            operaionDelegate.noActiveUser()
        }
    }
    
    func registerUser(user:User, activityTimeTracker:ActivityTimeTracker){
        let userRows  = [
            activities : user.activitiesValues,
            userType : user.profession,
            KCSUserAttributeGivenname : user.firstName,
            KCSUserAttributeSurname : user.lastName,
            KCSUserAttributeEmail : user.email,
            
            ]
        KCSUser.userWithUsername(
            user.username,
            password:user.password,
            fieldsAndValues: userRows as [NSObject : AnyObject],
            withCompletionBlock: { (user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                if errorOrNil == nil {
                    self.operaionDelegate.onSuccess()
                    self.store.saveObject(
                        user,
                        withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                            if errorOrNil != nil {
                                //save failed
                                print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
                                self.operaionDelegate.onError("something gone wrong!")
                            } else {
                                //save was successful
                                print("Successfully saved event (id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                                self.operaionDelegate.onSuccess()
                            }
                        },
                        withProgressBlock: nil
                    )
                    self.saveActivitiesAndTimeAfterSuccesfulRegistration(activityTimeTracker)
                    //self.saveActivitiesAndGoalAfterSuccesfulRegistration(goal)

                } else {
                    self.operaionDelegate.onError(errorOrNil.description)
                }
            }
        )
        saveData()
    }
    
    func loginUser(userLogin:Login){
        KCSUser.loginWithUsername(
            userLogin.userName,
            password: userLogin.password,
            withCompletionBlock: { ( user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                if errorOrNil == nil {
                    self.operaionDelegate.onSuccess()
                    self.defaults.setValue(userLogin.userName, forKey: Constants.USERNAME)
                    self.defaults.synchronize()
                    self.operaionDelegate.onSuccess()
                } else {
                    self.operaionDelegate.onError("OOPS!")
                }
            }
        )
    }
    
    func saveActivitiesAndTimeAfterSuccesfulRegistration(activityTimeTracker:ActivityTimeTracker){
        
        timeStore.saveObject(
            activityTimeTracker,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                
                if errorOrNil != nil {
                    //save failed
                    print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
                    self.operaionDelegate.onError("something gone wrong!")
                } else {
                    //save was successful
                    print("Successfully saved event (id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                    self.operaionDelegate.onSuccess()
                }
            },
            withProgressBlock: nil
        )
    }
    
    
    func saveActivitiesAndGoalAfterSuccesfulRegistration(goal:GoalModelClass){
        
        activityGoal.saveObject(
            goal,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                
                if errorOrNil != nil {
                    //save failed
                    print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
                    self.operaionDelegate.onError("something gone wrong!")
                } else {
                    //save was successful
                    print("Successfully saved event (id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                    self.operaionDelegate.onSuccess()
                }
            },
            withProgressBlock: nil
        )
    }
    
    //added by Manasa for Storing the activities and times in Kinvey om Apr11
    func trackTime(time:ActivityTimeTracker){
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        let query = KCSQuery(onField: "userName", withExactMatchForValue: userValue)
        timeStore.removeObject(
            query,
            withDeletionBlock: { (deletionDictOrNil: [NSObject : AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil != nil {
                    //error occurred - add back into the list
                    NSLog("Delete failed, with error: %@", errorOrNil.localizedFailureReason!)
                } else {
                    //delete successful - UI already updated
                    NSLog("deleted response: %@", deletionDictOrNil)
                    self.timeStore.saveObject(
                        time,
                        withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                            if errorOrNil != nil {
                                //save failed
                                print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
                                self.operaionDelegate.onError("something gone wrong!")
                            } else {
                                //save was successful
                                print("Successfully saved event (id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                                self.operaionDelegate.onSuccess()
                            }
                        },
                        withProgressBlock: nil
                    )
                }
            },
            withProgressBlock: nil
        )
    }
    
    func retrieveTime(){
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        let query = KCSQuery(onField: "userName", withExactMatchForValue: userValue)
        timeStore.queryWithQuery(
            query,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil == nil{
                    let userObject =   objectsOrNil[0] as! ActivityTimeTracker
                    self.operaionDelegate.sendTimeValues!(userObject)
                }
            },
            withProgressBlock: nil
        )
    }
    
    func retrieveGoals(){
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        let query = KCSQuery(onField: "userName", withExactMatchForValue: userValue)
        activityGoal.queryWithQuery(
            query,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                
                if errorOrNil == nil{
                    
                    if objectsOrNil.count == 0 {
                     
                        self.operaionDelegate.onError("OOPS!")
                       return
                    }
                    let userObject =   objectsOrNil[0] as! GoalModelClass
                    self.operaionDelegate.sendActivityGoals!(userObject)
                }
            },
            withProgressBlock: nil
        )
    }
    
    
    //    //added by manasa on mar31 for fetching data
    //
    //
    //    func retrieveData(){
    //
    //        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
    //      print(userValue)
    //        let query = KCSQuery(onField: "username", withExactMatchForValue: userValue)
    //        print(query)
    //
    //        //var activities:[String]!
    //        store.queryWithQuery(
    //            query,
    //            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
    //                print(objectsOrNil.count)
    //
    //                if errorOrNil == nil{
    //
    //                  let userObject =   objectsOrNil[0] as! User
    //                    print(userObject.activitiesValues)
    //                    print(userObject.firstName)
    //
    //                    self.operaionDelegate.sendValues!(userObject)
    //
    //                    //activities = userObject.activitiesValues!
    //
    //
    //                }
    //
    //            },
    //            withProgressBlock: nil
    //        )
    //        //return activities
    //    }
    //
    //
    //    func updatedActivities(userActivity:UpdatedActivities){
    //        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
    //        let query = KCSQuery(onField: "username", withExactMatchForValue: userValue)
    //        activityStore.removeObject(
    //            query,
    //            withDeletionBlock: { (deletionDictOrNil: [NSObject : AnyObject]!, errorOrNil: NSError!) -> Void in
    //                if errorOrNil != nil {
    //                    //error occurred - add back into the list
    //                    NSLog("Delete failed, with error: %@", errorOrNil.localizedFailureReason!)
    //                } else {
    //                    //delete successful - UI already updated
    //                    NSLog("deleted response: %@", deletionDictOrNil)
    //                }
    //            },
    //            withProgressBlock: nil
    //        )
    //        activityStore.saveObject(
    //            userActivity,
    //            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
    //
    //                if errorOrNil != nil {
    //                    //save failed
    //                    print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
    //                    self.operaionDelegate.onError("something gone wrong!")
    //                } else {
    //                    //save was successful
    //                    print("Successfully saved event (id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
    //                    self.operaionDelegate.onSuccess()
    //                }
    //            },
    //            withProgressBlock: nil
    //        )
    //
    //    }
    //
    //
        func saveUpdatedActivities(user:User){
    
            let userValue = defaults.valueForKey(Constants.USERNAME) as! String
    
            let query = KCSQuery(onField: "username", withExactMatchForValue: userValue)
    
            //deleting values of logged in user.
    
            store.removeObject(
                query,
                withDeletionBlock: { (deletionDictOrNil: [NSObject : AnyObject]!, errorOrNil: NSError!) -> Void in
                    if errorOrNil != nil {
                        //error occurred - add back into the list
    
                        NSLog("Delete failed, with error: %@", errorOrNil.localizedFailureReason!)
                    } else {
                        //delete successful - UI already updated
                        self.store.saveObject(
                            user,
                            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                                print("save fucntion")
                                if errorOrNil != nil {
                                    //save failed
    
                                    print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
                                    self.operaionDelegate.onError("something gone wrong!")
                                } else {
                                    print("save successful")
                                    //save was successful
                                    self.operaionDelegate.onSuccess()
                                }
                            },
                            withProgressBlock: nil
                        )
    
                        NSLog("deleted response: %@", deletionDictOrNil)
                    }
                },
                withProgressBlock: nil
            )
            print("*****************After delete in kinvey \(user.username)")
            //storing the new data into kinvey
    
        }
    
    func retrieveForSettings(){
        
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        let query = KCSQuery(onField: "username", withExactMatchForValue: userValue)
        store.queryWithQuery(
            query,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil == nil{
                    let userObject =   objectsOrNil[0] as! User
                    self.operaionDelegate.retrieveProfile!(userObject)
                }
            },
            withProgressBlock: nil
        )
        //return activities
    }
    
    func trackGoal(goal:GoalModelClass){
        
        
            let userValue = defaults.valueForKey(Constants.USERNAME) as! String
            
            let query = KCSQuery(onField: "userName", withExactMatchForValue: userValue)
            
            //deleting values of logged in user.
            
            activityGoal.removeObject(
                query,
                withDeletionBlock: { (deletionDictOrNil: [NSObject : AnyObject]!, errorOrNil: NSError!) -> Void in
                    if errorOrNil != nil {
                        //error occurred - add back into the list
                        
                        NSLog("Delete failed, with error: %@", errorOrNil.localizedFailureReason!)
                    } else {
                        NSLog("deleted response: %@", deletionDictOrNil)
                        //delete successful - UI already updated
                        self.activityGoal.saveObject(
                            goal,
                            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                                print("save fucntion")
                                if errorOrNil != nil {
                                    //save failed
                                    
                                    print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
                                    self.operaionDelegate.onError("something gone wrong!")
                                } else {
                                    print("save successful")
                                    //save was successful
                                    self.operaionDelegate.onSuccess()
                                }
                            },
                            withProgressBlock: nil
                        )
                        
                    }
                },
                withProgressBlock: nil
            )
            
            
        
    }
    
    
    
    
    
}