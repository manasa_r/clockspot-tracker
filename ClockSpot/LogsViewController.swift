//
//  LogsViewController.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/12/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import UIKit
import Charts

class LogsViewController: UIViewController, OperationProtocol {
 //created for dummy data
    @IBOutlet var chartTapGesture: UITapGestureRecognizer!
   
    var activityTimeTracker:ActivityTimeTracker!
    @IBOutlet weak var pieChartView: PieChartView!
    var activityTimes:[String:String] = [:]
    var activity:[String]!
    var kinveyOperations:KinveyOperations!
    var hours:[Double]!
    var timeHHMMSS:[String]!
    var times:[Double]!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        print("I am in view did load of logs")
        kinveyOperations = KinveyOperations(operationProtocol: self)
        // Do any additional setup after loading the view, typically from a nib.
        activity = []
        timeHHMMSS = []
        hours = [20.0, 4.0, 6.0]
        times = []
        
        pieChartView.noDataTextDescription = " Start an Activity"
        pieChartView.noDataText = " "
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //create pie chart view controller
    
    func setChart(dataPoints: [String], value: [Double]) {
        
        
        var dataEntries: [ChartDataEntry] = []
        var activityNames:[String] = []
        
        var sumValues:Double = 0.0
        for var i = 0; i < value.count ; i+=1 {
            sumValues += value[i]
        }
        
        if sumValues != 0.0 {
            var values:[Int] = []
            for var i = 0; i < value.count ; i+=1 {
                values.append((Int((value[i] * 100)/sumValues)))
                
            }
            
            for i in 0..<dataPoints.count {
                if values[i] != 0{
                    let dataEntry = ChartDataEntry(value: Double(values[i]), xIndex: i)
                    
                    activityNames.append(dataPoints[i])
                    dataEntries.append(dataEntry)
                    print("  i am a data entry \(dataEntries)")
                }
            }
            
            
            let pieChartDataSet = PieChartDataSet(yVals: dataEntries, label: "Activity")
            // let pieChartData = PieChartData(xVals: activityNames, dataSet: pieChartDataSet)
            print("  i am  a activity name \(activityNames)")
            
            
            
            //  pieChartView.data = pieChartData
            
            pieChartView.descriptionText = ""
            
            
            pieChartDataSet.colors = ChartColorTemplates.colorful()
            
            let pieChartData = PieChartData(xVals: activityNames, dataSet: pieChartDataSet)
            pieChartView.data = pieChartData
            
            
            pieChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .EaseInBounce)
            pieChartView.gestureRecognizerShouldBegin(chartTapGesture)
        }
        else{
            // noDataLabel.text = " No activities to show"
            pieChartView.noDataTextDescription = " Start an Activity"
        }
        
        //pieChartView.ge
        
    }
    
    func onError(message: String) {
        // displayAlertControllerWithFailure("OOPS!", message:"Login Failed")
        
    }
    func onSuccess() {
        //displayAlertControllerWithTitle("Success", message:"Login Successful")
    }
    func noActiveUser() {
        //
    }
    
    func loginFailed() {
        //
    }
    func sendTimeValues(time: ActivityTimeTracker) {
        self.activityTimes =  time.activityTimes
        getActivitiesTimes()
        print("In logs view controller")
        
    }
    func getActivitiesTimes(){
        
        activity = []
        timeHHMMSS = []
        times = []
        for act in activityTimes.keys{
            activity.append(act)
        }
        
        for time in activityTimes.values{
            print(" Time \(time)")
            timeHHMMSS.append(time)
        }
        
        for i in 0 ..< activityTimes.values.count {
            let seperateTime = timeHHMMSS[i].componentsSeparatedByString(":")
            let hours = Double(Int(seperateTime[0])! * 60)
            let sec = Double(Double(seperateTime[2])! / 60)
            let sumTime = Double(hours + Double(seperateTime[1])! + sec)
            times.append(sumTime)
        }
        print("Logs activities and times sum \(activity) \(timeHHMMSS) \(times)")
        
        setChart(activity, value:( times))
        
    }
    override func viewWillAppear(animated: Bool) {
        kinveyOperations.retrieveTime()
    }
    
    
    
    
}
