//
//  NewGoalViewController.swift
//  ClockSpot
//
//  Created by Pinky on 4/14/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import UIKit

class NewGoalViewController: UIViewController, OperationProtocol {

    
    var kinveyOperations:KinveyOperations!
    let defaults = NSUserDefaults.standardUserDefaults()
    var activityTimes:[String:String] = [:]

    var goalVC:GoalViewController!
    var activities:[String] = []
    
    var activityIsPresent:Bool = false
    
    var activitiesGoal:GoalModelClass!
    var activityGoal:[String:String]!
    
    @IBOutlet weak var goalTimeTF: UITextField!
    @IBOutlet weak var activityNameTF: UITextField!
    
    let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
    override func viewDidLoad() {
       
        super.viewDidLoad()
        activitiesGoal = GoalModelClass()
        activitiesGoal.activityGoal = [:]
        kinveyOperations = KinveyOperations(operationProtocol: self)

        print("In view didLoad new goal")
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        
        // Do any additional setup after loading the view, typically from a nib.
        
            }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        //created
    }

    override func viewWillAppear(animated: Bool) {
        
        kinveyOperations.retrieveTime()

        
    }
    
    
    
    
    @IBAction func saveBTN(sender: AnyObject) {
        
        for act in activityTimes.keys{
            activities.append(act)
        }
        print(activityNameTF.text)
        for activity in activities{
            
            if activityNameTF.text != activity{
                
                activityIsPresent = false
                
                
            }else{
                
                activityIsPresent = true
                break
            }
            
        }
        
        if activityIsPresent{
            
            activitiesGoal.userName = defaults.valueForKey(Constants.USERNAME) as! String
            activitiesGoal.activityGoal[activityNameTF.text!] = goalTimeTF.text!
            
            print("In New Goal View Controller \(activitiesGoal.activityGoal)")
            
            kinveyOperations.trackGoal(activitiesGoal)

        }else{
            displayAlertControllerWithTitle("OOPS!", message:"Please add this activity before you set the goal")
        }
    }
    
    
    
    func sendActivityGoals(goal: GoalModelClass) {
            activitiesGoal.activityGoal = goal.activityGoal
            print(activitiesGoal.activityGoal)
    }

    
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
                                                                    message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:{(action:UIAlertAction)->Void in }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
        
        
    }

    
    func onError(message: String) {
        // displayAlertControllerWithFailure("OOPS!", message:"Login Failed")
        
    }
    func onSuccess() {
        let goal =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("goal") as! GoalViewController
        self.navigationController?.pushViewController(goal, animated: true)
    }
    func noActiveUser() {
        //
    }
    
    func loginFailed() {
        //
    }
    
    func sendValues(user: User) {
        //
    }
    func sendTimeValues(time: ActivityTimeTracker) {
        self.activityTimes =  time.activityTimes
        kinveyOperations.retrieveGoals()

        
        
    }
    
    
    @IBAction func cancelBTN(sender: AnyObject) {
        let backToGoalView =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("goal") as! GoalViewController
        self.navigationController?.pushViewController(backToGoalView, animated: true)

        
    }
    
}
