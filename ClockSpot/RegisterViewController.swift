//
//  RegisterViewController.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/13/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate,OperationProtocol{
    
    
    @IBOutlet weak var firstNameTF: UITextField!
    
    @IBOutlet weak var lastNameTF: UITextField!
    
    @IBOutlet weak var userNameTF: UITextField!
    
    @IBOutlet weak var profesionPicker: UIPickerView!
    
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var emailErrorLBL: UILabel!
    
    @IBOutlet weak var passwordErrorLBL: UILabel!

    @IBOutlet weak var confirmPasswordTF: UITextField!
    
    @IBOutlet weak var emailTF: UITextField!
    var kinvey:KinveyOperations!


    var activities:Activities = Activities()
    var user:User!
    var activityTimeTracker:ActivityTimeTracker!
    var actvitiesWithGoals:GoalModelClass!
    var selectedValue:String!
    var profession = ["STUDENT","LAWYER","DOCTOR","PROFESSOR"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationController?.navigationBarHidden = true;
self.navigationItem.setHidesBackButton(true, animated:true)
        kinvey = KinveyOperations(operationProtocol: self)
        activityTimeTracker = ActivityTimeTracker()
        actvitiesWithGoals = GoalModelClass()
        activityTimeTracker.activityTimes = [:]
        actvitiesWithGoals.activityGoal = [:]
            emailErrorLBL.hidden = true
            passwordErrorLBL.hidden = true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationItem.title = "Registration"
    }

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return profession.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return profession[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        // use the row to get the selected row from the picker view
        // using the row extract the value from your datasource (array[row])
         selectedValue = profession[row]
      
    }

    @IBAction func registerUser(sender: AnyObject) {
        
        
        var valid = true
        
        
        
        if firstNameTF.text!.isEmpty{
            
            firstNameTF.layer.borderWidth = 1
            firstNameTF.layer.borderColor = UIColor.redColor().CGColor
            //displayAlertControllerWithTitleforFailure("Fail!", message:"Enter First Name")
            
        }
        else
        {
            
            firstNameTF.layer.borderColor = UIColor.whiteColor().CGColor
        }
        if lastNameTF.text!.isEmpty{
            
            lastNameTF.layer.borderWidth = 1
            lastNameTF.layer.borderColor = UIColor.redColor().CGColor
            //displayAlertControllerWithTitleforFailure("Fail!", message:"Enter Last Name")
            
        }
        else
        {
            lastNameTF.layer.borderColor = UIColor.whiteColor().CGColor
        }
        if userNameTF.text!.isEmpty{
            
            userNameTF.layer.borderWidth = 1
            userNameTF.layer.borderColor = UIColor.redColor().CGColor
            //displayAlertControllerWithTitleforFailure("Fail!", message:"Enter UserName")
            
        }
        else
        {
            userNameTF.layer.borderColor = UIColor.whiteColor().CGColor
        }
        
        
        if emailTF.text!.isEmpty{
            
            emailTF.layer.borderWidth = 1
            emailTF.layer.borderColor = UIColor.redColor().CGColor
            //displayAlertControllerWithTitleforFailure("Fail!", message:"Enter Password")
            
        }
        else{
            
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
            let range = emailTF.text!.rangeOfString(emailRegEx, options:.RegularExpressionSearch)
            let result = range != nil ? true : false
            
            if result == false
            {
                valid = false
                emailErrorLBL.textColor = UIColor.redColor()
                emailErrorLBL.text = "Please enter a valid Email Id"
                emailErrorLBL.hidden = false
                emailTF.text = ""
                emailTF.layer.borderWidth = 1
                emailTF.layer.borderColor = UIColor.redColor().CGColor
            }
            else
            {
                valid = true
                emailErrorLBL.hidden = true
                emailTF.layer.borderColor = UIColor.whiteColor().CGColor
            }
        }
        
        //        let validPassword:Bool = isValidPassword(passwordTF.text!)
        //        if validPassword == false {
        //            error += "Please enter valid Password:should have one lower case letter, one upper case letter, one digit, 6-13 length, and no spaces"+"\n"
        //        }
        //
        
        if passwordTF.text!.isEmpty{
            
            passwordTF.layer.borderWidth = 1
            passwordTF.layer.borderColor = UIColor.redColor().CGColor
            //displayAlertControllerWithTitleforFailure("Fail!", message:"Enter Password")
            
        }
        else
        {
            let passwordRegEx = "[A-Z0-9a-z.!@#$%^&*()]{6,13}"
            let range = passwordTF.text!.rangeOfString(passwordRegEx, options:.RegularExpressionSearch)
            let result = range != nil ? true : false
            
            if result == false
            {
                passwordErrorLBL.textColor = UIColor.redColor()
                valid = false
                passwordErrorLBL.text = "Please enter a password between 6-13 characters"
                passwordErrorLBL.hidden = false
                passwordTF.text = ""
                passwordTF.layer.borderWidth = 1
                passwordTF.layer.borderColor = UIColor.redColor().CGColor
            }
            else{
                valid = true
                passwordErrorLBL.hidden = true
                passwordTF.layer.borderColor = UIColor.whiteColor().CGColor
            }
        }
        
        
        
        if confirmPasswordTF.text!.isEmpty{
            
            confirmPasswordTF.layer.borderWidth = 1
            confirmPasswordTF.layer.borderColor = UIColor.redColor().CGColor
            //displayAlertControllerWithTitleforFailure("Fail!", message:"Enter Password")
            
        }
        else
        {
            let passwordRegEx = "[A-Z0-9a-z.!@#$%^&*()]{6,13}"
            let range = confirmPasswordTF.text!.rangeOfString(passwordRegEx, options:.RegularExpressionSearch)
            let result = range != nil ? true : false
            
            if result == false
            {
                valid = false
                passwordErrorLBL.text = "Password length must be between 6-13\n"
                confirmPasswordTF.text = ""
                confirmPasswordTF.layer.borderWidth = 1
                confirmPasswordTF.layer.borderColor = UIColor.redColor().CGColor
            }
            else{
                valid = true
                confirmPasswordTF.layer.borderColor = UIColor.whiteColor().CGColor
            }
        }
        
        
        
        
        
        
        if passwordTF.text! != confirmPasswordTF.text!{
            
            displayAlertControllerWithTitleforFailure("Fail!", message:"Your passwords don't match \n")
            
            passwordTF.text = ""
            confirmPasswordTF.text = ""
            passwordTF.layer.borderWidth = 1
            passwordTF.layer.borderColor = UIColor.redColor().CGColor
            confirmPasswordTF.layer.borderWidth = 1
            confirmPasswordTF.layer.borderColor = UIColor.redColor().CGColor
            
        }
        
        
        
        
        
        
        
        if !firstNameTF.text!.isEmpty && !lastNameTF.text!.isEmpty && !userNameTF.text!.isEmpty && !emailTF.text!.isEmpty && !passwordTF.text!.isEmpty && !confirmPasswordTF.text!.isEmpty && valid==true
        {
            if selectedValue == nil
            {
                selectedValue = "STUDENT"
            }
            user = User(firstName: firstNameTF.text!, lastName: lastNameTF.text!, username: userNameTF.text!, password: passwordTF.text!, confirmPassword: confirmPasswordTF.text!, profession: selectedValue, email: emailTF.text!)
            
            user.activitiesValues = activities.updateActivities(user.profession)
            
            activityTimeTracker.userName = userNameTF.text!
            
            print(user.activitiesValues)
            for i in 0..<user.activitiesValues.count{
            
                activityTimeTracker.activityTimes[user.activitiesValues[i]] = "00:00:00"
            }
            //actvitiesWithGoals.userName = userNameTF.text!
            //actvitiesWithGoals.activityGoal[""] = ""
            //kinvey.registerUser(user,activityTimeTracker: activityTimeTracker, goal:actvitiesWithGoals)
            kinvey.registerUser(user,activityTimeTracker: activityTimeTracker)

            displayAlertControllerWithTitle("Success", message:"Registration Successful")
            
            
        }
    }
    
    func onSuccess() {
        //
    }
    
    func onError(message: String) {
        //
    }
    
    func noActiveUser() {
        //
    }
    
    func loginFailed() {
        //code
    }
    
    
    
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
                                                                    message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:  { action in let login =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("loginView") as! LoginViewController
                self.navigationController?.pushViewController(login, animated: true) }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
        
        
        
    }
    func displayAlertControllerWithTitleforFailure(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
                                                                    message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:{(action:UIAlertAction)->Void in }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
        
        
    }
    
    
    
    @IBAction func cancelBTN(sender: AnyObject) {
        
        let login =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("loginView") as! LoginViewController
        self.navigationController?.pushViewController(login, animated: true)

    }
    
    //    func isValidPassword(password: String) -> Bool {
    //            let passwordRegex = "^.(?={6,}(?=.[a-z])(?=.[A-Z]))"
    //
    //    return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluateWithObject(password)
    //
    //}
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}