//
//  SettingsViewController.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/12/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true)

         // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
         //created
    }
    
    @IBAction func logout(sender: AnyObject) {
        
        let backToLogin =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("loginView") as! LoginViewController
        self.navigationController?.pushViewController(backToLogin, animated: true)
    }
    
    
    @IBAction func updateProfileBTN(sender: AnyObject) {
        let toProfile =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("profile") as! UpdateProfileViewController
        self.navigationController?.pushViewController(toProfile, animated: true)

        
    }
}