//
//  UpdateProfileViewController.swift
//  ClockSpot
//
//  Created by Karupakala,Rajesh on 3/15/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import UIKit

class UpdateProfileViewController: UIViewController, OperationProtocol {

    var user:User!
    var kinveyOperations:KinveyOperations!
    var firstName:String!
    var lastName:String!
    var userName:String!
    var profession:String!
    var email:String!
    var password:String!
    var confirmPassword:String!
    var activities:[String]!
    
    @IBOutlet weak var userNameTF: UITextField!
    
    @IBOutlet weak var lastNameTF: UITextField!
    
    @IBOutlet weak var firstNameTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Settings"
        
        kinveyOperations = KinveyOperations(operationProtocol: self)
        
        kinveyOperations.retrieveForSettings()
    
        //self.navigationItem.setHidesBackButton(true, animated: true)

        
        // Do any additional setup after loading the view, typically from a nib.
        //commit
    }
    
    
    @IBAction func saveBTN(sender: AnyObject) {
        firstName = firstNameTF.text
        lastName = lastNameTF.text
        userName = user.username
        profession = user.profession
        email = user.email
        password = user.password
        confirmPassword = user.confirmPassword
        activities = user.activitiesValues
      
       user = User(firstName: firstName, lastName: lastName, username: userName, password: "", confirmPassword: "", profession: profession, email: email)
        print("LastName in save button \(lastName)")
        user.activitiesValues = activities
        kinveyOperations.saveUpdatedActivities(user)
        
        
        displayAlertControllerWithTitleforFailure("Success", message: "Profile has been updated")
    }
    
    
    
    func onError(message: String) {
        // displayAlertControllerWithFailure("OOPS!", message:"Login Failed")
        
    }
    func onSuccess() {
        //displayAlertControllerWithTitle("Success", message:"Login Successful")
    }
    func noActiveUser() {
        //
    }
    
    func loginFailed() {
        //
    }
    
    func sendValues(user:User) {
        
       //
        
    }
    
    func sendTimeValues(time:ActivityTimeTracker){
    
    
    }
    func retrieveProfile(user:User){
        
        self.user = user
        
        firstNameTF.text = user.firstName
        
        lastNameTF.text = user.lastName
        
    }
    
    func displayAlertControllerWithTitleforFailure(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
                                                                    message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
           
            handler:  { action in let settings =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("settings") as! SettingsViewController
                self.navigationController?.pushViewController(settings, animated: true) }))
            
            
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
        
        
    }

    
}
