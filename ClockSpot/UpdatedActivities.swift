//
//  UpdatedActivities.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 4/12/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import Foundation

class UpdatedActivities:NSObject{
    
    var userName:String!
    var activities:[String]!
    var entityId:String?
    
    
    init(userName:String,activities:[String]){
        
        self.userName = userName
        self.activities = activities
        
        
    }
    
    override init() {
        super.init()
    }
    
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "userName":"userName",
            "activities":"activities"
        ]
    }
    
}