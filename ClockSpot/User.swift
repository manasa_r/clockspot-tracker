//
//  User.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/13/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import Foundation



class User :NSObject {
    
   // var activities:Activities = Activities()
    var firstName:String!
    var lastName:String!
    var username:String!
    var password:String!
    var confirmPassword:String!
    var profession:String!
    var email:String!
    var activitiesValues:[String]!
    var activitiesTimes:[Float]!
    var entityId:String?
    
    init(firstName:String, lastName:String, username:String, password:String, confirmPassword:String, profession:String, email:String){
        self.firstName = firstName
        self.lastName = lastName
        self.username = username
        self.password = password
        self.confirmPassword = confirmPassword
        self.profession = profession
        self.email = email
        
        
        print("lastName in UsrClass \(self.lastName) \(lastName)")
        //activitiesValues = activities.updateActivities(profession)
        //print("********** \(activitiesValues)")
        
    }
    
    
  
    override init() {
        super.init()
    }

    
    init(username:String)
    {
       self.username = username
    }
    
    init(email:String)
    {
        self.email = email
    }
    
    
   
    init(password:String)
    {
        self.password = password
    }
    
    init(confirmPassword:String)
    {
        self.confirmPassword = confirmPassword
    }
    
    init(profession:String)
    {
        self.profession = profession
    }
    
    
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "firstName" : "first_name",
            "lastName" : "last_name",
            "username" : "username",
            "password" : "password",
            "profession" : "profession",
            "email" : "email",
            "activitiesValues":"activitiesValues"
        ]
    }
    
    
    
    func display(user:User) {
    
      print("\(user.firstName) \(user.lastName) \(user.profession) \(user.activitiesValues)")
    
    }
    
}